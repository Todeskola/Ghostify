package de.ghostify.web.controllers;

import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

/**
 * Created by DomZei
 * 2018-12-23 23:24
 */
@Controller
public class IndexController {

    @RequestMapping(value = {"", "/index"}, method = RequestMethod.GET)
    public String sayHello(Model model) {

        return "index";
    }
}
