package de.ghostify.web;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.domain.*;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.*;

/**
 * Created by DomZei
 * 2018-12-23 22:43
 */
@SpringBootApplication
@EnableJpaRepositories
@EntityScan(basePackages = "de.ghostify.model.*")
@ComponentScan(basePackages = "de.ghostify.backend.*")
public class GhostifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GhostifyApplication.class);
    }
}
