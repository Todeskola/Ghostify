package de.ghostify.backend.views.authentication;

import com.vaadin.flow.component.button.*;
import com.vaadin.flow.component.orderedlayout.*;
import com.vaadin.flow.component.textfield.*;
import com.vaadin.flow.router.*;
import de.ghostify.backend.views.authentication.components.*;

/**
 * Created by DomZei
 * 2018-12-30 10:09
 */

@Route("login")
@PageTitle(value = "Login")
public class LoginView extends FlexLayout {

    private TextField userName;
    private PasswordField password;
    private Button loginBtn;
    private Button forgotPasswordBtn;

    public LoginView() {

        this.buildUi();
    }

    private void buildUi() {

        setSizeFull();
        setClassName("login-view");

        LoginFormComponent loginForm = new LoginFormComponent().buildLoginForm();

        FlexLayout centeringLayout = new FlexLayout();
        centeringLayout.setSizeFull();
        centeringLayout.setJustifyContentMode(JustifyContentMode.CENTER);
        centeringLayout.setAlignItems(Alignment.CENTER);
        centeringLayout.add(loginForm);

        add(centeringLayout);
    }
}
