package de.ghostify.backend.views.authentication.components;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.*;
import com.vaadin.flow.component.formlayout.*;
import com.vaadin.flow.component.orderedlayout.*;
import com.vaadin.flow.component.textfield.*;

/**
 * Created by DomZei
 * 2018-12-30 10:14
 */
public class LoginFormComponent extends Component {

    private TextField userName;
    private PasswordField password;
    private Button loginBtn;
    private Button forgotPwdBtn;

    public LoginFormComponent buildLoginForm() {

        FormLayout loginForm = new FormLayout();

        loginForm.setWidth("310px");

        loginForm.addFormItem(userName = new TextField(), "Benutzername oder Email");
        userName.setWidth("15em");
        userName.setValue("Benutzername oder Email");
        loginForm.add(new Html("<br/>"));
        loginForm.addFormItem(password = new PasswordField(), "Passwort");
        password.setWidth("15em");

        HorizontalLayout buttons = new HorizontalLayout();
        loginForm.add(new Html("<br/>"));
        loginForm.add(buttons);

        buttons.add(loginBtn = new Button("Login"));
        //TODO: implement login
        loginBtn.addThemeVariants(ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY);

        buttons.add(forgotPwdBtn = new Button("Forgot password?"));
        //TODO: implement workflow
        forgotPwdBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        return this;
    }

    public TextField getUserName() {
        return userName;
    }

    public PasswordField getPassword() {
        return password;
    }
}
