package de.ghostify.persistence.models.user;

/**
 * Created by DomZei
 * 2018-12-23 22:27
 */
public enum Gender {

    FEMALE, MALE;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
