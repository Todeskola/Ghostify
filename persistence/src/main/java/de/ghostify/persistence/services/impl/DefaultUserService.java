package de.ghostify.persistence.services.impl;

import de.ghostify.persistence.dao.*;
import de.ghostify.persistence.models.user.*;
import de.ghostify.persistence.services.*;
import java.util.*;
import javax.annotation.*;
import org.springframework.stereotype.*;

/**
 * Created by DomZei
 * 2018-12-24 00:23
 */
@Service
public class DefaultUserService implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public boolean isUserExistingByUserName(String userName) {

        return userDao.existsByUserName(userName);
    }

    @Override
    public boolean isUserExistingByEmail(String email) {

        return userDao.existsByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {

        return userDao.findAll();
    }

    @Override
    public User getUserByUserName(String userName) {

        Optional<User> optionalUser = userDao.findUserByUserName(userName);

        return optionalUser.orElse(null);
    }

    @Override
    public User getUserByEmail(String email) {

        Optional<User> optionalUser = userDao.findUserByEmail(email);

        return optionalUser.orElse(null);
    }

    @Override
    public void deleteUserById(Long id) {

        userDao.deleteById(id);
    }

    @Override
    public void deleteUser(User user) {

        userDao.delete(user);
    }

    @Override
    public void saveUser(User user) {

        userDao.save(user);
    }

    @Override
    public boolean updateUser(User changedUser) {

        Optional<User> user = userDao.findById(changedUser.getId());

        if(!user.isPresent()) {

            return false;
        }

        userDao.save(changedUser);

        return true;
    }
}
