package de.ghostify.persistence.services;

import de.ghostify.persistence.models.user.*;
import java.util.*;

/**
 * Created by DomZei
 * 2018-12-24 00:23
 */
public interface UserService {

    boolean isUserExistingByUserName(String userName);
    boolean isUserExistingByEmail(String email);

    List<User> getAllUsers();
    User getUserByUserName(String userName);
    User getUserByEmail(String email);

    void deleteUserById(Long id);
    void deleteUser(User user);

    void saveUser(User user);

    boolean updateUser(User changedUser);
}
