package de.ghostify.persistence.dao;

import de.ghostify.persistence.models.user.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

/**
 * Created by DomZei
 * 2018-12-23 22:35
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {

    Optional<User> findUserByUserName(String userName);
    Optional<User> findUserByEmail(String email);
    boolean existsByUserName(String userName);
    boolean existsByEmail(String email);
}
