package de.ghostify.core.services.impl;

import de.ghostify.core.converter.user.*;
import de.ghostify.core.data.user.*;
import de.ghostify.core.services.*;
import de.ghostify.persistence.models.user.*;
import de.ghostify.persistence.services.*;
import java.util.*;
import javax.annotation.*;
import org.springframework.stereotype.*;

/**
 * Created by DomZei
 * 2018-12-24 00:56
 */
@Service
public class DefaultUserDataService implements UserDataService {

    @Resource
    private UserService userService;

    @Resource
    private UserConverter userConverter;

    @Resource
    private UserReverseConverter userReverseConverter;

    @Override
    public boolean isUserNameExisting(String userName) {

        return userService.isUserExistingByUserName(userName);
    }

    @Override
    public boolean isEmailExisting(String email) {

        return userService.isUserExistingByEmail(email);
    }

    @Override
    public List<UserData> getAllUsers() {

        List<UserData> userDataList = new ArrayList<>();

        for(User user : userService.getAllUsers()) {

            userDataList.add(userConverter.convert(user));
        }

        return userDataList;
    }

    @Override
    public UserData getUserDataByUserName(String userName) {

        User user = userService.getUserByUserName(userName);

        return user != null ? userConverter.convert(user) : null;
    }

    @Override
    public UserData getUserByEmail(String email) {

        User user = userService.getUserByEmail(email);

        return user != null ? userConverter.convert(user) : null;
    }

    @Override
    public void removeUser(UserData userData) {

        if(userData.getId() != null) {

            this.removeUserById(userData.getId());
            return;
        }

        User user = userReverseConverter.convert(userData);
        userService.deleteUser(user);
    }

    @Override
    public void removeUserById(Long id) {

        userService.deleteUserById(id);
    }

    @Override
    public void createUser(UserData userData) {

        User user = userReverseConverter.convert(userData);

        userService.saveUser(user);
    }

    @Override
    public boolean updateUser(UserData userData) {

        User user = userReverseConverter.convert(userData);

        return userService.updateUser(user);
    }
}
