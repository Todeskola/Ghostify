package de.ghostify.core.services;

import de.ghostify.core.data.user.*;
import java.util.*;

/**
 * Created by DomZei
 * 2018-12-24 00:54
 */
public interface UserDataService {

    boolean isUserNameExisting(String userName);
    boolean isEmailExisting(String email);

    List<UserData> getAllUsers();
    UserData getUserDataByUserName(String userName);
    UserData getUserByEmail(String email);

    void removeUser(UserData userData);
    void removeUserById(Long id);

    void createUser(UserData userData);

    boolean updateUser(UserData userData);
}
