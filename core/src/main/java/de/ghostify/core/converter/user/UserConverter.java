package de.ghostify.core.converter.user;

import de.ghostify.core.data.user.*;
import de.ghostify.persistence.models.user.*;
import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

/**
 * Created by DomZei
 * 2018-12-24 00:38
 */
@Component
public class UserConverter implements Converter<User, UserData> {

    @Override
    public UserData convert(User source) {

        if(source == null) {

            return null;
        }

        UserData userData = new UserData();

        userData.setId(source.getId());
        userData.setUserName(source.getUserName());
        userData.setPassword(source.getPassword());
        userData.setFirstName(source.getFirstName());
        userData.setLastName(source.getLastName());
        userData.setEmail(source.getEmail());
        userData.setGender(source.getGender().toString());
        userData.setBirth(source.getBirth());
        userData.setActive(source.isActive());
        userData.setCreated(source.getCreated());
        userData.setUpdated(source.getUpdated());

        return userData;
    }
}
