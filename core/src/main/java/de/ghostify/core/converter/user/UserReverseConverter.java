package de.ghostify.core.converter.user;

import de.ghostify.core.data.user.*;
import de.ghostify.persistence.models.user.*;
import org.springframework.core.convert.converter.*;
import org.springframework.stereotype.*;

/**
 * Created by DomZei
 * 2018-12-24 00:47
 */
@Component
public class UserReverseConverter implements Converter<UserData, User> {

    @Override
    public User convert(UserData source) {

        if(source == null) {
            return null;
        }

        User user = new User();

        user.setUserName(source.getUserName());
        user.setPassword(source.getPassword());
        user.setFirstName(source.getFirstName());
        user.setLastName(source.getLastName());
        user.setEmail(source.getEmail());
        user.setGender(Gender.valueOf(source.getGender()));
        user.setBirth(source.getBirth());
        user.setActive(source.isActive());

        return user;
    }
}
